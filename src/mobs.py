import flags

""" This class is for the mobs of the game. All mobs will generally be found here """
class Mob():
    def __init__(self, name, description, hp, atk, deff, room=None, inventory=[]):
        self.name = name
        self.inventory = inventory
        self.description = description
        self.hp = hp
        self.atk = atk
        self.deff = deff
        self.room = room
        self.is_alive = True

    def die(self):
        self.is_alive = False
        self.description = ""
        self.name = ""
        self.room = None

    def attack(self):
        return self.atk

    def defense(self):
        return self.deff

    def damage(self, dmg):
        if dmg >= 0:
            self.hp -= dmg

    def heal(self, amt):
        self.hp += amt

class NPC(Mob):
    def __init__(self, name, description, room, quest_list=[], inv=[]):
        super().__init__(name, description, 1, 1, 1, room, inv)
        self.quest_list = quest_list

    def __str__(self):
        return self.name

    def chat(self, *args, **kwargs):
        raise NotImplementedError

class Test_NPC(NPC):
    def __init__(self, room):
        super().__init__("Test NPC", "This is a test person!", "Just a test!", room)
        self.quest_list = []
        self.talk_flag = flags.Test_Talk_Flag_First_Talk

    def chat(self, *args, **kwargs):
        player = kwargs['player']
        input_data = kwargs['input_data']
        intro_text = "Do you like pie?\n1. Yes\n2. No "
        if len(player.flags) <= 0:
            player.flags.append(self.talk_flag(self))
            return intro_text
        else:
            for flag in player.flags:
                if isinstance(flag, flags.Talk_Flag):
                    if isinstance(flag, flags.Test_Talk_Flag_First_Talk) and (self == flag.read()):
                        for flag in player.flags:
                            if isinstance(flag, flags.Test_Talk_Flag):
                                can_read = flag.can_read()
                                selection = flag.read()
                                if can_read:
                                    if selection == 1:
                                        return "You like pie...right?"
                                    elif selection == 2:
                                        return "Get away from me you pie-hating freak!"
                        if input_data:
                            if "1" in str(input_data) or "yes" in str(input_data).lower():
                                player.flags.append(flags.Test_Talk_Flag(1))
                                return "Good! I will remember that!"
                            elif "2" in str(input_data) or "no" in str(input_data).lower():
                                player.flags.append(flags.Test_Talk_Flag(2))
                                return "Do you not like fun?!"
                        return intro_text
        return "This is an error message!"

    def die(self):
        self.hp = 1

    def attack(self):
        pass

    def defense(self):
        pass

    def damage(self, dmg):
        pass

class Player(Mob):
    def __init__(self, name, description, race, hp=10, atk=10, deff=10, inv=[], quests=[], killed=[], flags=[]):
        super().__init__(name, description, hp, atk, deff)
        self.race = race
        self.quests = quests
        self.killed = killed
        self.flags = flags

    def die(self):
        pass

    def set_room(self, new_room):
        if new_room:
            self.room = new_room

    def attack(self):
        new_atk = self.atk
        for item in inv:
            if item.equipped and isinstance(item, weapon):
                new_atk += item.atk_buff
        if new_atk > 0:
            new_atk = 0
        return new_atk

    def defense(self):
        new_def = self.deff
        for item in inv:
            if item.equipped and isinstance(item, defense):
                new_def += item.def_buff
        if new_def > 0:
            new_def = 0
        return new_def
