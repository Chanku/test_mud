import multiprocessing
import sys
import global_values
import socket
import world

""" This Module acts as the "listener" for all connections
     on the server. """

def start():
    """ This starts the server, accepts connections, and sets them up for usage. """
    world_read_pipe, world_write_pipe = multiprocessing.Pipe(False)
    world_process = multiprocessing.Process(target=world.World_Start, args=(world_read_pipe,))
    world_process.start()
    host = ''
    port = 223

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    try:
        s.bind((host, port))
    except socket.error as e:
        sys.exit(str(e))

    s.listen(5)

    while True:
        client, addr = s.accept()
        read_from_client, send_to_world = multiprocessing.Pipe(False)
        read_from_world, send_to_client = multiprocessing.Pipe(False)
        c_proc = multiprocessing.Process(target=world.connect, args=(client, addr, send_to_world, read_from_world))
        c_proc.start()
        world_write_pipe.send((
                               (read_from_client, send_to_client),
                               c_proc.pid))
    s.shutdown(socket.SHUT_RDWR)
    s.close()


if __name__ == "__main__":
    start()
