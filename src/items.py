""" This Module holds all game-objects, such as items. """
class Game_Object():
    def __init__(self, name, description, value):
        self.name = name
        self.description = description
        self.value = value

    def pick_up(self):
        return self


