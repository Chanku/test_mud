class Room():
    def __init__(self, name, description, exits, mobs=[], npcs=[], inventory=[]):
        self.name = name
        self.description = description
        self.exits = exits #A dict with, 'n', 's', 'e', 'w' as keys
        self.mobs = mobs
        self.npcs = npcs
        self.base_mobs = mobs
        self.inventory = inventory
        self.base_inventory = inventory

    def respawn_items(self):
        self.inventory = self.base_inventory

    def check_for_dead(self):
        for mob in self.mobs():
            if not mob.is_alive:
                self.mobs.remove(mob)

class Test_Room(Room):
    def __init__(self):
        import mobs
        exits = {'n':None, 's':None, 'e':None, 'w':None}
        super().__init__("Test Room", ("This is a testing room.\n"
                                       "You shouldn't be here"), exits, npcs=[mobs.Test_NPC(self)])
