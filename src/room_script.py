""" This module exists to read and assembly the world through world scripts """
WORLD_FILE = False #NOTE: This is used to mark whether a giant world-script is used or individual room scripts
SCRIPT_LOCATION = './scripts/rooms/' #This is set to make things a bit simpler later on and in the code base
def read_script():
    if world_file:
        pass
    elif not world_file:
       file_list = find_files()
       parse_files(file_list)

def find_files():
    room_list = []
    for room in os.listdir(SCRIPT_LOCATION):
        if room.endswith('.troom'):
            room_list.append(room)
    return room_list

def parse_files(file_list):
    #room {
    #  description = ""
    #  n_exit: {room_file}
    #  s_exit: {room_file}
    #  e_exit: {room_file}
    #  w_exit: {room_file}
    #  mobs: []
    #  start: False
    #}
    for rfile in file_list:




