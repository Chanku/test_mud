""" This module defines the command API and how commands are to be added. """

command_list = []
class Command():
    """ This is the Command class, this is where all commands are derived from """
    def __init__(self, name, short_description, long_description, help_text, alias=[None]):
        self.name = name
        self.alias = alias
        self.short_description = short_description
        self.long_description = long_description
        self.help_text = help_text

    def execute(self, player_data, input_data):
        raise NotImplementedError

    def setup(self):
        raise NotImplementedError

    def get_help_text(self):
        t = ""
        i = 0
        alias = ""
        while i < len(self.name):
            t = '{}{}'.format(t, '=')
            i += 1
        i = 0
        for string in self.alias:
            if not string:
                alias = "None"
                break
            alias = "{}{},".format(alias, string)
        help_string = ("{n}\n"
                       "{x}\n"
                       "{ht}\n"
                       "\n"
                       "short description: {sd}\n"
                       "long description: {ld}\n"
                       "alias: {ali}\n").format(n=self.name, x=t, sd=self.short_description, ht=self.help_text,
                                                  ld=self.long_description, ali=alias)
        return help_string

class Move(Command):
    def __init__(self):
        ld = ("This command moves the player in the direction they want to go.\n"
               "The Aliases must be used though in order to actually make it work.")
        super().__init__('move', 'move to another room', ld, ld, alias=['n', 's', 'e', 'w'])

    def setup(self):
        pass

    def execute(self, player_data, input_data):
        sp_data = input_data.split(" ")
        if 'n' == sp_data[0]:
            player_data.set_room(player_data.room.exits['n'])
        elif 's' == sp_data[0]:
            player_data.set_room(player_data.room.exits['s'])
        elif 'e' == sp_data[0]:
            player_data.set_room(player_data.room.exits['e'])
        elif 'w' == sp_data[0]:
            player_data.set_room(player_data.room.exits['w'])
        elif 'move' == sp_data[0]:
            return "Please use the aliases"
        return player_data.room.description


class Help(Command):
    def __init__(self):
        ld = "This command dislays a command's help!"
        super().__init__("help", 'get help!', ld, ld)

    def setup(self):
        pass

    def execute(self, player_data, input_data):
        sp_data = input_data.split(" ")
        for command in command_list:
            try:
                if sp_data[1] == command.name or sp_data[1] in command.alias:
                    print('x')
                    return command.get_help_text()
            except:
                return "Not enough Arguments!\n Syntax: help [command]"
        return "Not a valid command"

class Talk(Command):
    def __init__(self):
        ld = "allows you to talk to an NPC"
        super().__init__("talk", "talk", ld, ld)

    def setup(self):
        pass

    def execute(self, player_data, input_data):
        sp_data = input_data.split(" ")
        if len(sp_data) == 1:
            npc_list = ""
            if len(player_data.room.npcs) > 0 and player_data.room.npcs[0]:
                for npc in player_data.room.npcs:
                    npc_list = "{}{}. {} - {}\n".format(npc_list, player_data.room.npcs.index(npc), npc.name, npc.description)
                npc_list[:len(npc_list) - 1]
            else:
                npc_list = "There are no NPCs here"
            return npc_list
        else:
            try:
                npc = player_data.room.npcs[int(sp_data[1])]
                return '{} said: "{}"'.format(npc.name, npc.chat(player=player_data, input_data=input_data))
            except IndexError:
                return "There are no NPCs here"

class Attack(Command):
    def __init__(self):
        ld = "attacks a specified enemy"
        super().__init__("attack", "attack!", ld, ld)

    def setup(self):
        pass

    def execute(self, player_data, input_data):
        sp_data = input_data.split(" ")
        if len(sp_data) == 1:
            enemy_list = ""
            if len(player_data.room.mobs) > 0 and player_data.room.mobs[0]:
                for enemy in player_data.room.mobs:
                    enemy_list = "{}{}. {}: hp - {} atk - {} def - {}\n".format(enemy_list, player_data.room.mobs.index(enemy), enemy.name,
                                                                                enemy.hp, enemy.atk, enemy.deff)
                enemy_list[:len(enemy_list) - 1]
            else:
                enemy_list = "There are no mobs here"
            return enemy_list
        else:
            try:
                enemy = player_data.room.mobs[int(sp_data[1])]
                enemy_attack = enemy.attack()
                enemy_defense = enemy.defense()
                player_attack = player_data.attack()
                player_defense = player_data.defense()
                enemy.damage(player_attack - enemy_defense)
                player.damage(enemy_attack - player_defense)
                return ("You dealt: {}\n"
                        "You took: {}\n"
                        "{}'s Health: {}\n"
                        "Your Health: {}").format(str(player_attack - enemy_defense), str(enemy_attack - player_defense),
                                                    enemy.name, str(enemy.hp), str(player_data.hp))
            except IndexError:
                return "There are no mobs here"

class Quit(Command):
    #NOTE: This class doesn't really house the Quit Command.
    #      Due to the nature of the command it is handled by the World itself.
    #      As such this class is a placeholder class to allow Quit to be accessed
    #       through the Help Command at this time.
    def __init__(self):
        ld = "Disconnects you from the game"
        super().__init__("quit", "leave!", ld, ld)

    def setup(self):
        pass

    def execute(self, *args, **kwargs):
        pass

def check_for_command(str_data, player_data):
    """finds the command and then executes it"""
    print(global_values.pr_con.player_connections)
    data = str_data.split(" ")[0].lower()
    for command in command_list:
        if data == command.name or data in command.alias:
            return command.execute(player_data, str_data)
    else:
        return "Not a command"

def register_command(command_obj):
    """registers a command"""
    x = command_obj()
    x.setup()
    command_list.append(x)

register_command(Move)
register_command(Help)
register_command(Quit)
register_command(Attack)
register_command(Talk)
import global_values
