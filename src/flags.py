flag_list = []

class Flag():
    def __init__(self,name, read_limit): #read_limit = None/0 means it is never removed
        self._name = name
        self._read_limit = read_limit
        self.__read_count = 0
        self.__delete_on_tick = False #Flag for removing the flag on the tick

    def __str__(self):
        return self._name

    def _check_remove(self):
        return self.__delete_on_tick

    def can_read(self):
        """ This method checks to make sure you are supposed to be able to
            read the flag's data. It can read up to one plus the deletion number """
        self.__read_count += 1
        if self._read_limit and self._read_limit == (self.__read_count + 1):
            self.__delete_on_tick = True
        elif self.__delete_on_tick:
            return False
        print('a')
        return True
    
    def read(self):
        raise NotImplementedError

class Room_Flag(Flag):
    def __init__(self, name, read_limit, room_object):
        super().__init__(name, read_limit)
        self.room_object = room_object

class Talk_Flag(Flag):
    def __init__(self, name, read_limit):
        super().__init__(name, read_limit)

class Test_Talk_Flag(Talk_Flag):
    def __init__(self, selection):
        super().__init__("Likes Pie", 10)
        self._selection = selection

    def read(self):
        return self._selection

class Test_Talk_Flag_First_Talk(Talk_Flag):
    def __init__(self, npc):
        super().__init__("Has Interacted!", 0)
        self.npc = npc

    def read(self):
        return self.npc


def register_flags(flag_object):
    flag_list.append(flag_object)

register_flags(Test_Talk_Flag)
