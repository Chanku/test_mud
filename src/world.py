import commands
import global_values
import items
import mobs
import multiprocessing
import os
import rooms
import signal
import socket
import sys
import time


class World():
    def __init__(self, rooms=[], pipes=(None,None)):
        self.rooms = rooms
        self.players = {}
        self.run = True
        self.stack = []
        self.last_tick = time.time()
        self.Read_Pipe = pipes[0]
        self.Write_Pipe = pipes[1]
        self.tick_read_pipe, self.tick_write_pipe = multiprocessing.Pipe(False)
        self.tick_process = multiprocessing.Process(target=self.tick, args=(30, self.tick_write_pipe,))
        self.tick_process.start()

    def update(self):
        if not self.tick_process.is_alive():
            self.tick_process.join(0.5)
            if len(self.stack) > 0:
                for s_action in self.stack:
                    try:
                        if not s_action['action'] == 'quit':
                            self.players[s_action['player']][0][1].send(commands.check_for_command(s_action['action'],
                                                                                                   self.players[s_action['player']][1]))
                        elif s_action['action'] == 'quit':
                            self.remove_player(s_action['player'])
                    except KeyError as e:
                        print(str(e))
                self.stack = []
                for key in self.players:
                    for flag in self.players[key][1].flags:
                        if flag._check_remove():
                            self.players[key][1].flags.remove(flag)
            self.current_time = self.tick_read_pipe.recv()
            self.tick_process = multiprocessing.Process(target=self.tick, args=(30, self.tick_write_pipe,))
            self.tick_process.start()
        for key in self.players:
            if self.players[key][0][0].poll(0.0005):
                print(key)
                self.stack.append({'player':key, 'action':self.players[key][0][0].recv()})

    def tick(self, aps, write_pipe):
        interval = 60 / aps
        current_time = time.time()
        delta = current_time - self.last_tick
        if delta < interval:
            time.sleep(interval - delta)
        write_pipe.send(current_time)
        sys.exit(0)

    def remove_player(self, player):
        print('removing player')
        send_to_client = self.players[player][0][1]
        del self.players[player]
        try:
            send_to_client.send('okay')
        except BrokenPipeError:
            pass
        time.sleep(1)

        #NOTE: This shouldn't be done, and should be generalized.  #TODO: Remove linux specific code
        try:
            with open("/proc/{}/status".format(global_values.get_pid(player)), 'r') as f:
                f.read()
                try:
                    os.kill(global_values.get_pid(player), 0)
                    os.kill(global_values.get_pid(player), 9)
                    try:
                        os.kill(global_values.get_pid(player), 0)
                    except OSError:
                        pass
                except OSError:
                    pass
        except FileNotFoundError:
            pass

        global_values.remove_player(player)
        del send_to_client

    def add_player(self, player, ip, player_connection_obj, pipe_tuple, pid):
        name = player.name
        read_from_client = pipe_tuple[0]
        send_to_client = pipe_tuple[1]
        global_values.update_player_list(name, ip, player_connection_obj, pid)
        self.players[name] = (
                              (read_from_client, send_to_client),
                              player)
        player.set_room(self.rooms[0])
        player_connection_obj.send("{}\n=====\n{}\n".format(player.room.name, player.room.description).encode('utf-8'))

def game_player_input(player, connection, send_to_world):
    while True:
        rData = connection.recv(2048)
        data = rData.decode('utf-8').strip('\r\n')
        if data.strip() == "quit":
            send_to_world.send(data)
            sys.exit(0)
        else:
            send_to_world.send(data)
    sys.exit(1)

def game_player_output(player, connection, read_from_world):
    while True:
        if read_from_world.poll():
            print('o')
            try:
                connection.send("{}\r\n".format(read_from_world.recv()).encode('utf-8'))
            except BrokenPipeError:
                sys.exit(0)
    sys.exit(1)

def game(player, connection, send_to_world, read_from_world):
    safe_exit = False
    proc_go = multiprocessing.Process(target=game_player_output, args=(player, connection, read_from_world,))
    proc_gi = multiprocessing.Process(target=game_player_input, args=(player, connection, send_to_world,))
    proc_go.start()
    proc_gi.start()
    while True:
        if not proc_gi.is_alive():
            if proc_gi.exitcode or proc_gi.exitcode == 0:
                print('sexit')
                safe_exit = True
                break
            elif proc_gi.exitcode and proc_gi.exit_code == 1:
                print('nexit')
                proc_gi = multiprocessing.Process(target=game_player_input, args=(connection, send_to_world,))
                proc_gi.start()
        if not proc_go.is_alive() and proc_go.exit_code == 1:
            print('restart')
            proc_go = multiprocessing.Process(target=game_player_output, args=(player, connection, read_from_world,))
            proc_go.start()
        elif not prog_go.is_alive() and proc_go.eit_code == 0:
            safe_exit = True
            send_to_world.send('quit')
            break
    if not safe_exit:
        sys.exit(1)
    proc_go.terminate()
    proc_gi.terminate()
    sleep(5)
    proc_go.join()
    proc_gi.join()
    return 0

def connect(connection_obj, ip_addr, send_to_world, read_from_world):
    connection_obj.send("CONNECTED!\n".encode('utf-8'))
    connection_obj.send("Welcome to Sapein's Test Mud!\n".encode('utf-8'))
    connection_obj.send("Please select an option below!\n".encode('utf-8'))
    connection_obj.send("1. Login\n".encode('utf-8'))
    connection_obj.send("2. Exit\n".encode('utf-8'))
    rResponse = connection_obj.recv(2048)
    response = rResponse.decode('utf-8')
    if '1' in response or 'login' in response.lower():
        connection_obj.send("Please enter your name: ".encode('utf-8'))
        rName = connection_obj.recv(2048)
        connection_obj.send(("Please select your race\n"
                             " 1. Human\n").encode('utf-8'))
        rRace = connection_obj.recv(2048)
        race = rRace.decode('utf-8').strip('\r\n')
        if '1' in race or 'human' in race.lower():
            race = 'human'
        player = mobs.Player(rName.decode('utf-8').strip('\r\n'), 'dilf', race)
        package = {'player':player, 'conn':connection_obj, 'ip':ip_addr}
        send_to_world.send(package)
        _ = game(player, connection_obj, send_to_world, read_from_world)

        while True:
            if read_from_world.poll():
                resp = read_from_world.recv()
                if resp == 'okay':
                    break

        connection_obj.send("Goodbye\n".encode('utf-8'))
    if '2' in response or 'quit' in response.lower() or 'exit' in response.lower():
        connection_obj.send("Goodbye\n".encode('utf-8'))
    connection_obj.shutdown(socket.SHUT_RDWR)
    connection_obj.close()
    return 0

def World_Start(read_pipe):
    world = World([rooms.Test_Room()])
    while world.run:
        if read_pipe.poll(0.0005):
            player_data_tuple = read_pipe.recv()
            player_pipe_tuple = player_data_tuple[0]
            if player_pipe_tuple[0].poll(10):
                player_data = player_pipe_tuple[0].recv()
                world.add_player(player_data['player'], player_data['ip'],
                                 player_data['conn'], player_pipe_tuple, player_data_tuple[1])
            else:
                print("player connection terminated!")
                os.kill(player_data_tuple[1], 9)
                del player_data_tuple
        world.update()
