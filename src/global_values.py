""" This module is to hold global values that may or maynot change. """
class _Private_Constants():
    """ This class should not be used outside of this module.
        No information from this class should be given to players"""
    def __init__(self):
        self.player_connections = {} # This is used with [username]:([connection_object], [PID])
        self.connected_ip = {} # This is used with [username]:[ip]

def update_player_list(playername, ip_addr, player_connection_obj, player_process_obj):
    """ This function is mainly used to update all of the records.
        Players should only be added through this command."""
    pr_con.player_connections[playername] = (player_connection_obj, player_process_obj)
    pr_con.connected_ip[playername] = ip_addr

def get_player(playername):
    """ This function is mainly to be used to get the connections of a player."""
    return pr_con.player_connections[playername], pr_con.connected_ip[playername]

def get_pid(playername):
    """ This function is used to return the PID of a player. """
    return pr_con.player_connections[playername][1]

def remove_player(playername):
    """ This function is to remove a player from the connections. """
    del pr_con.player_connections[playername]
    del pr_con.connected_ip[playername]

#The following should not be accessed directly
pr_con = _Private_Constants()
